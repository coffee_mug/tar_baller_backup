# Tar Baller Backup
This is a simple run and relax script that backs-up a linux system using tar. The Backups are saved by default in /home/Backups.

## Setup
- Clone or download this repo
- Put the script wherever you want
- `sudo chmod +x backup.sh` to make it executable

## Usage
Whenever you want to create a back-up, just run the script and wait for it to finish. That's it.
To run it use one of the following: 
- `sudo bash backup.sh`
- `sudo ./backup.sh`
- `sudo backup.sh` 

## Details
- The utility will create a Backups dir and all other needed structure on its own. The structure is as follows:
    ```
    \home
    ├── tar_baller_backups
    │   └── 2018
    |       └── July
    │          ├── 03
    |          |   └── [name of host].BU.2018-07-03.13-23-42.tar.gz    <-- Backup
    |          |   └── [name of host].log.2018-07-03.13-23-42.txt      <-- Log of backup
    │          └── 04
    |              └── [name of host].BU.2018-07-04.13-23-42.tar.gz
    |              └── [name of host].log.2018-07-04.13-23-42.txt
    ├── user1
    ├── user2
    ├── user3
    ```
- The log file contains a list of all files that were backed up
- You can add more excludes to exclude any files/dirs you don't want backed up

## WARNINGS
> If you have a directory in your home named 'tar_baller_backups' this will cause issues. It is extremely unlikely that running the script will result in file loss but the structure of the backups will be messed up
> The log *will list all the files that are in your backup*
