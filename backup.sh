#!/usr/bin/env bash

TBB_VERSION="0.1.3"
DATE=`date '+%Y-%m-%d.%H-%M-%S'`
ROOT_BACKUP_DIR='/home/tar_baller_backups/'                         # Directory to store all the backups in
YEAR_BACKUP_DIR=`date +"%Y"`'/'                                     # Directory to group backups by year
MONTH_BACKUP_DIR=$ROOT_BACKUP_DIR$YEAR_BACKUP_DIR`date +"%B"`'/'    # Directory to group backups by year month
DATE_BACKUP_DIR=$MONTH_BACKUP_DIR`date +"%d"`'/'                    # Directory to group backups by each day of the month
BACKUP_FILE=$DATE_BACKUP_DIR$HOSTNAME'.BU.'$DATE'.tar.gz'           # File path of the backup
BACKUP_LOG=$DATE_BACKUP_DIR$HOSTNAME'.log.'$DATE'.txt'              # File path to the log of that backup
BACKUPEE='/home/'                                                   # Location to backup


echo "===================================================="
echo "  _______"                             
echo " |__   __|"                            
echo "    | | __ _ _ __"                     
echo "    | |/ _\` | \'__|"                    
echo "    | | (_| | |"                       
echo "  __|_|\__,_|_| _"                     
echo " |  _ \      | | |"                    
echo " | |_) | __ _| | | ___ _ __"           
echo " |  _ < / _\` | | |/ _ \ '__|"          
echo " | |_) | (_| | | |  __/ |"             
echo " |____/ \__,_|_|_|\___|_|           _" 
echo " |  _ \           | |              | |"
echo " | |_) | __ _  ___| | ___   _ _ __ | |"
echo " |  _ < / _\` |/ __| |/ / | | | '_ \| |"
echo " | |_) | (_| | (__|   <| |_| | |_) |_|"
echo " |____/ \__,_|\___|_|\_\\___,_| .__/(_)"
echo "                             | |"      
echo "                             |_|"      
echo "===================================================="

echo "Tar Baller Backup version $TBB_VERSION"
echo "---------------------------------------------------------------------------"

echo "Creating backup in $DATE_BACKUP_DIR"
echo "Starting backup..."
# Wait for 3 seconds so you can cancel it if you want to
sleep 3
mkdir -p $DATE_BACKUP_DIR

# Write header info to the log
echo "Backup of home triggered by $USER on $HOSTNAME" > $BACKUP_LOG
echo "Date of backup: $DATE" >> $BACKUP_LOG
echo "Tar Baller Backup version: $TBB_VERSION" >> $BACKUP_LOG
echo "---------------------------------------------------------------------------" tee -a $BACKUP_LOG

tar					    \
--exclude=*journal.txt                      \
--exclude=*.git-credentials                 \
--exclude=*snap/                            \
--exclude=*.cache                           \
--exclude=*pia.sh                           \
--exclude=*.com.privateinternetaccess.vpn   \
--exclude=*.pia_manager/		    \
--exclude=*Trash                            \
--exclude=*.tar.gz                          \
--exclude=*.tar                             \
--exclude=*.zip				    \
--exclude="*lost+found"                     \
--exclude=$ROOT_BACKUP_DIR                  \
--exclude=*Downloads/                       \
--exclude=*.bash_history                    \
--exclude=*.mozilla	                    \
--exclude=*.git-credentials                 \
--exclude=*snap                             \
--exclude=*.steam			    \
--exclude=*digikam4db			    \
--exclude=*recognition.db		    \
--exclude=*thumbnails-digikam.db	    \
-cvpzf "$BACKUP_FILE" --one-file-system     \
$BACKUPEE | tee -a $BACKUP_LOG

echo "---------------------------------------------------------------------------"
echo "Backup complete. Have a nice day."
